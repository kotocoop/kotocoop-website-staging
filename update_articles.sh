#!/bin/bash

cd $(dirname $0)

git pull

for l in english finnish; do
	echo language $l

	rsync -r ~/articles/$l/ content/$l/articles/
	rm content/$l/articles/*.docx

	for f in content/$l/articles/*; do
		echo article $f
		mv "$f" `echo $f | tr ' ' '_'`	
	done

	git add content/$l/articles/*.md
done

git commit -m "articles updated"
git push
