---
title: Auroora garden planning and tracking 
date: 2024-03-04 20:00:00 +0000
layout: post
extimages:
- https://content.kotocoop.org/pad/tayr57s3mx303rositjssgltl3ij0h3fff1l93gvt1hkbo/7917c837-6f29-42ec-ba98-c9c03e2b928e.png
tags: [ "auroora", "earth_restoring_lifestyle" ]

---

### Auroora garden planning and tracking

We added a new page to keep track of plants we have planted and the overall plan seen in the image above.

[Check it out here!](https://kotocoop.org/units/auroora/garden/)

[Here is information about the place](https://kotocoop.org/units/auroora/).
