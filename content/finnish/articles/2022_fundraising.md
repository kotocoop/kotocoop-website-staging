---

title: Post-scarcity Support Ry fundraising

#image: /img/units/auroora/auroora_outside_2022.jpg

images: 
- /img/units/auroora/auroora_outside_2022.jpg

aliases:

- /possu_ry_fundraising/

date: "2022-08-22 15:30:00 +0000"
expiryDate: "2022-11-11"

---

## Post-scarcity Support Ry

Collection number: RA/2022/1137

### Bank transfer information

- IBAN: FI81 7997 7993 5279 23
- BIC: HOLVFIHH
- Recipient: Post-Scarcity Support ry
- Reference: 01229

