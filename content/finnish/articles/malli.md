---
title: Koto osuuskunnan toimintamalli
date: 2024-10-20T17:51:00.000Z
image: /img/blog/blog-post-1.jpg
aliases:
  - /malli
---
## Tarkoitus

Koto osuuskunnan tarkoituksena on kestävän, niukkuuden jälkeisen (Post-scarcity) yhteiskunnan mahdollistavien tekniikoiden ja sosiaalisten rakenteiden kehittäminen sekä tämän kehitystyön mahdollistaminen. Tätä tarkoitusta edistääksemme pyrimme luomaan toimintamalleja, joilla saamme ihmisiä mukaan sekä turvaamaan heille toimeentulon. Tällä tavoin pyrimme ylläpitämään kehitys- ja tutkimustyötä kestävästi.

## Osa laajempaa yhteisöä

Yhteisönä koemme olevamme osa laajempaa globaalia liikehdintää kohti kestävää maailmaa. Keskeisiä periaatteita joita jaamme kyseisen liikkeen kanssa, ja jotka ohjaavat toimintaamme, ovat: kestävä kehitys, hyvinvointi keskeisyys, yhteisöllisyys, open-source ja jakamistalous. Pyrimme toimimaan mahdollisimman paljon yhteistyössä tahojen kanssa, joiden kanssa jaamme samoja periaatteita sekä yhteisiä tavoitteita. Yhteistyön kautta saamme tehokkuutta toimintaamme, kun työskentelemme yhdessä yhteisten päämäärien eteen.

Kehitystyömme ytimessä on elämää ylläpitävien ja toimeentulon mahdollistavien tekniikoiden kehittäminen “pienteollisesta” näkökulmasta. Pyrimme hyödyntämään teknologian mahdollistamaa tehokkuutta, mutta emme kuitenkaan lähde kehittämään teollisen mittakaavan ratkaisuja  - - ainakaan toistaiseksi. Kehitämme esimerkiksi ruuan ja sähkön tuotantoon liittyviä sovelluksia, yleistä tuotantoa kuten materiaalien kierrätystä, valmistusta, jalostusta ja käsittelyä, sekä infrastruktuuriin, rakentamiseen, asentamiseen ja ylläpitoon liittyviä sovelluksia. Oikeastaan pyrimme kehittämään ja tutkimaan kaikkea yhteiskuntaa ylläpitävää tekniikkaa pienemmässä mittakaavassa. Kehitystyössämme hyödynnämme mahdollisimman paljon open-source lähteitä ja jo kehitettyjä sovelluksia. Kaikki kehityksemme tulokset jaamme myös avoimesti internetissä.

## Organisaatio

Toimintamallinamme on organisoitua erilaisiin yksiköihin ja tiimeihin. Tiimit työskentelevät eri aihealueiden parissa, ja tarjoavat mahdollisuuden osallistua osuuskunnan toimintaan oman osaamisen ja kiinnostusten mukaisesti. Pääsääntöisesti kehitystyö toimii tällaisten tiimien kautta. Yksiköt taas ovat pääosin paikallisesti toimivia kokonaisuuksia, niissä osuuskunta pyrkii tarjoamaan toimeentuloa jäsenilleen. Yksiköissä sovelletaan ja hyödynnetään osuuskunnan kehittämää tekniikkaa. Jotkut yksiköt saattavat muistuttaa “high-tech ekokyliä”, kun taas toiset voisivat olla enemmän kaupunkiympäristöön soveltuvia ratkaisuja. Nämä toimintamallit eivät ole pysyviä vaan niiden kehittäminen mahdollisimman toimiviksi, kestäviksi ja tehokkaiksi on myös keskeinen osa osuuskunnan toimintaa. Uusien ihmisten myötä pyrimme kehittämään toimintamalleja, jotka pystyvät mahdollistamaan mahdollisimman hyvin kaikkien kiinnostuneiden osallistumisen.

# Talous

Taloudessaan osuuskunta pyrkii kasvattamaan omavaraisuusastetta asteittain toiminnan laajenemisen myötä. Osuuskunta tarvii kuitenkin rahatuloja toiminnan ylläpitämiseksi ja etenkin sen kasvattamiseksi ja kehittämiseksi. Keskeisenä tulonlähteenä tulee todennäköisesti toimimaan osuuskunnalle laskutettu työ sekä osuuskunnan tuotteiden myyminen. Rahatuloja voidaan saada myös esimerkiksi lahjoituksista, jäsenmaksuista, joukkorahoituksesta tai projekteihin haetuista rahoituksista.
Keskeisenä tavoitteenamme on onnistua luomaan positiivinen kierre, jossa pystymme hyötymään ja soveltamaan kehittämiämme tekniikoita, ja niiden avulla laajentamaan ja parantamaan toimintaamme entisestään, jolloin saamme kehitettyä entistä parempia tekniikoita. 

## Positiivinen takaisinkytkentä

Tekniikan avulla pyrimme tuottamaan osuuskunnan jäsenille mahdollisimman korkeaa elintasoa mahdollisimman vähällä työllä ja mahdollisimman kestävästi. Kehitystyön tuloksena pyrimme vähentämään toimintamme taloudelliseen ylläpitoon kuluvaa työn määrää, jolloin meille jää enemmän aikaa kehitykselle ja enemmän resursseja uusille ihmisille sekä toiminnan laajentamiseen.
Samalla kun kehitämme uusia tekniikoita, jolla voimme tarjota toimeentuloa jäsenillemme, vähennämme asteittan riippuvuuttamme ulkoisesta kaupankäynnistä ja rahataloudesta. Vaikka emme välttämättä pyri täydelliseen omavaraisuuteen, pystymme mahdollisimman korkealla omavaraisuusasteella pitämään toimintamme kestävällä pohjalla, kun emme ole riippuvaisia osuuskunnalle tulevista rahavirroista. Mitä vähemmän olemme niistä riippuvaisia, sitä vähemmän meidän tulee niitä myös ylläpitää.

## Koska olemme onnistuneet?

Olemme onnistuneet tavoitteessamme, kun osuuskunta pystyy tarjoamaan ihmisille mahdollisuuden käyttää aikansa osuuskunnan tarkoituksen mukaisesti elämää ylläpitävän tekniikan kehittämiseen ilman, että heidän tulee pelätä oman toimeentulonsa puolesta.
Mitä pidemmälle tässä tavoitteessa pääsemme, sitä paremmin pystymme myös tarjoamaan mahdollisuuksia nauttia tekniikan tuomasta toimeentulon turvasta pelkästää ylläpitämällä, ilman tarvetta kehitystyöhön osallistumiselle.
Vielä muutama asia osuuskunnan laillisesta rakenteesta. Osuuskunnan toimintaa ohjaa osuuskunnan säännöt.
Näiden sääntöjen mukaan osuuskunta on voittoa tavoittelematon. Osuuskunta ei jaa osinkoa, vaan kaikki mahdollinen ylijäämä sijoitetaan omaan toimintaan. Osuuskunnan operatiivisena johtona toimii hallitus, joka on aluksi yksijäseninen. Osuuskunnan toiminnasta päättää osuuskuntakokous, jossa jokaisella osuuskunnan jäsenellä on äänioikeus. Osuuskunnan toimintaa ohjaa ja määrittelee Suomen laki.
Koto osuuskunta on perustettu mahdollistamaan työskentely kestävän maailman eteen. Onnistuaksemme tavoitteessamme, tarvitsemme kuitenkin teitä. Osuuskunnan toiminta muovaantuu sen mukaan minkälaisia taitoja ja kiinnostuksen kohteita osallistujilla on. Tarvitsemme mukaan ihmisiä, joilla riittää intoa tehdä asioita vaikka aika ei aina riitäkään. Pyrimme järjestämään jokaiselle tapoja tehdä niitä asioita osuuskunnan hyväksi.
