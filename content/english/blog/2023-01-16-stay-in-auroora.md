---
title: Stay in Auroora 2023
date: 2023-01-16 10:00:00 +0000
layout: post
images:
- img/blog/auroora2023.jpg
---

Our community is focused on creating a post-scarcity gift economy, and we are committed to supporting each other to find meaning and purpose in our work. We are building a community based on the principles of sustainability, transparency, permaculture, veganism, a scientific worldview and open source technology.

We are now looking for someone to inhabit our former village school in the country side, Auroora, for the growing season (e.g. six months from May to October) on a pay-what-you-can basis. We offer the use of one large bedroom (33 sqm, bed, wardrobe, desk), shared kitchen, dining room, shower room and toilet, as well as a car and an electric bike.

More information about the property is available here: <https://kotocoop.org/units/auroora/>

During the year some sections of the house will be renovated and we aim to complete the sauna rooms, basement workshop, more living space and at least a lightweight greenhouse.

The whole yard area is to be used in cooperation with the cooperative for the establishment of a garden and other activities. The harvest you feel is yours remains yours.

All residents must commit to the community rules, which are mainly related to respect for other residents and responsibility for the environment.

Visitors and people working on the project will live in other rooms all the time or from time to time, and we hope to have at least one person who can drive a car and can help with transport tasks, such as picking people up from the train station when there are no other people who can drive.

If you are interested in joining the community for the growing season, please fill out this form <https://kotocoop.org/auroora2023> and we will contact you to find out more and discuss the details.
