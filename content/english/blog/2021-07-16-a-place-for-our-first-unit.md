---
layout: post
title: 'We have purchased a place for our first unit!'
sub_heading: ''
date: 2021-07-16 13:00:00 +0000
tags: [ "auroora" ] 
images:
- /img/units/auroora/auroora_outside.jpg

related_posts: []

---

It is an 455 square meter school building built in the beginning of 20th century and it is located in Rääkkylä, Finland. Rääkkylä is located in eastern part of Finland and to the closest city it is about 60km drive. Closest train station is about 20km away at Kitee and from Helsinki to Kitee it is a 4.5h trip. The location in the middle of country side, lakes and forests offers a lot.

In its current state, the building can host 4-5 people and after renovation and organizing the rooms a little differently, a few more. Two of the rooms are quite big since one was a small school gymnasium and the other was in other common usage. Those rooms will be great places for an office or other workspaces. In the basement there is a workshop suitable for metal work, sauna, cool storage rooms for food and other storage rooms. It will be a great place for communal living!

The land area is a bit over one hectare. On the plot there is some forest, apple trees and a large outbuilding that used to be storage, garage and a cowshed.

Some work is still needed before we can call it a unit: unit contract has to be written and the building needs some fixing. Our first tenant will move in in the end of august. Currently no co-op members are moving in so the unit will be more distributed in nature, contact person living elsewhere.

If you wish to live in this unit or join following ones, fill this [questionnaire](https://kotocoop.org/joining_a_unit) and we will contact you. You can also join our teams in our community, if you want to help with for example technology, youtube content, communications or designing land usage. Our website has a contact form on the bottom of the first page <http://kotocoop.org/>

Photo by Riitta Makkonen