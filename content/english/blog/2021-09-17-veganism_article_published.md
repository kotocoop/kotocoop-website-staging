---
layout: post
title: 'Koto and veganism article'
sub_heading: ''
date: 2021-09-17 10:30:00 +0000
tags: []

images:
- /img/blog/sheep.jpg

related_posts: []

---

A vegan lifestyle is more ecological and also ethical toward other beings: it's all about reducing unnecessary suffering. Koto community will strive to spread information and live by example, both in terms of veganism and other societal and technical aspects.

Read the full article post on [here](https://kotocoop.org/articles/veganism/)
