---
title: Auroora - Garden
date: 2023-03-03 10:00:00
layout: singletask
aliases:
- /units/auroora/garden
- /task/auroora-garden
---

## Puutarhan suunnitelmia ja seurantaa - Garden plans and tracking

### ID system / tunnukset

Esim. 202310-TX-#Y, jossa

- 202310 on vuosiluku ja kuukausi jolloin hankinta tai istutus on tehty
- X on tyyppi numero
  - 1 - pensas
  - 2 - puumainen pensas
  - 3 - puu
- Y on juokseva numero

### Pihasuunnttelu

- Kansio [https://cloud.kotocoop.org/index.php/f/79359](https://cloud.kotocoop.org/index.php/f/79359)
- SVG [https://cloud.kotocoop.org/index.php/f/84575](https://cloud.kotocoop.org/index.php/f/84575)

#### Kuva

![](https://content.kotocoop.org/pad/tayr57s3mx303rositjssgltl3ij0h3fff1l93gvt1hkbo/7917c837-6f29-42ec-ba98-c9c03e2b928e.png)

## Aikajana

Marraskuu 2024

- Tarhapensasmustikka Vaccinium Viheraarni Aino
  - 2x
- Hunajamarja Lonicera caerulea var. Kamtschatica Viheraarni Siniczka
  - 2x
- Tarhapensasmustikka Viheraarni Vaccinium Northcountry
  - 1kpl
- Tyrni emikasvi Hippophae rhamnoides Viheraarni Persikkatyrni
  - 2kpl
- Pähkinäpensas Corylus avellana Viheraarni
  - 4kpl
- Kirsikka Prunus cerasus Viheraarni Suklaakirsikka
  - 1kpl
- Viiniköynnös Vitis Viheraarni Zilga
  - 2kpl
- Kääpiövuorimänty Pinus mugo var. Pumilio Viheraarni 25-30
  - 1kpl
- Päärynä Pyrus communis Viheraarni Lada
  - 1kpl
- Siperianhernepensas Caragana arborescens Viheraarni
  - 4kpl
- Tarhapensasmustikka Vaccinium Viheraarni Alvar
  - 1kpl

### Lokakuu 2023

![](https://content.kotocoop.org/pad/tayr57s3mx303rositjssgltl3ij0h3fff1l93gvt1hkbo/5ea19be1-e30a-4192-9ffc-e9c4ebe1a5c8.jpeg)

![](https://content.kotocoop.org/pad/tayr57s3mx303rositjssgltl3ij0h3fff1l93gvt1hkbo/5c63391d-b2d8-4537-8b35-db8f02073eae.jpeg)

- 202310-T1-#1
  - Tarhapensasmustikka (Vaccinium) 'Viheraarni Aino' - 1 kpl
  - Highbush Blueberry (Vaccinium) 'Viheraarni Aino' - Quantity: 1
  - Korkeus 1m, leveys 1m, Tuore/kostea, Aurinko
- 202310-T1-#2
  - Tarhapensasmustikka (Vaccinium) 'Viheraarni Alvar' - 1 kpl
  - Highbush Blueberry (Vaccinium) 'Viheraarni Alvar' - Quantity: 1
  - Korkeus 1m,  leveys 1m, Tuore/kostea, Aurinko
- 202310-T1-#3
  - Aronia Melanocarpa 'Marja-aronia' - 2 kpl
  - Aronia Melanocarpa - Quantity: 2
  - Korkeus 2m,  leveys 2m, Tuore/kostea, Aurinko
- 202310-T1-#4
  - Karviainen (Ribes) 'Viheraarni Lepaan Punainen' - 1 kpl
  - Gooseberry (Ribes) 'Viheraarni Lepaan Red' - Quantity: 1
  - Korkeus 2m, leveys 1.5m, Tuore/kostea, Aurinko
- 202310-T1-#5
  - Karviainen (Ribes) 'Viheraarni Hinnonmäen Keltainen' - 1 kpl
  - Gooseberry (Ribes) 'Viheraarni Hinnonmäen Yellow' - Quantity: 1
  - Korkeus 1.5m, leveys 1m, Tuore/kostea, Aurinko
- 202310-T2-#1
  - Tyrni emikasvi (Hippophae rhamnoides) 'Viheraarni Persikkatyrni' - 1 kpl
  - Sea Buckthorn female plant (Hippophae rhamnoides) 'Viheraarni Persikkatyrni' - Quantity: 1
  - Korkeus 2-3m, leveys 1.5m, Tuore/kostea, Aurinko
- 202310-T3-#1
  - Kirsikka (Prunus cerasus) 'Viheraarni Suklaakirsikka' - 1 kpl
  - Cherry (Prunus cerasus) 'Viheraarni Chocolate Cherry' - Quantity: 1
  - Korkeus 4m, leveys 4m
- 202310-T1-#6
  - Hunajamarja (Lonicera caerulea var. Kamtschatica) 'Viheraarni Hilma' - 2 kpl
  - Honeyberry (Lonicera caerulea var. Kamtschatica) 'Viheraarni Hilma' - Quantity: 2
  - Korkeus 1.5m, leveys 1.5m, Kuiva/Tuore, Puolivarjo/Aurinko
- 202310-T1-#7
  - Vadelma (Rubus idaeus) 'Viheraarni Muskoka' - 3 kpl
  - Raspberry (Rubus idaeus) 'Viheraarni Muskoka' - Quantity: 3
  - Korkeus 3m, leveys 1.5m, Kuiva/tuore, Aurinko
- 202310-T1-#8
  - Viherherukka (Ribes nigrum) 'Viheraarni Vilma' - 1 kpl
  - Green Currant (Ribes nigrum) 'Viheraarni Vilma' - Quantity: 1
  - Korkeus 2m, leveys 1.5m, Tuore/kostea, Aurinko
- 202310-T3-#2
  - Omenapuu (Malus domestica) 'Viheraarni Pirja' - 1 kpl
  - Apple Tree (Malus domestica) 'Viheraarni Pirja' - Quantity: 1
  - Korkeus 4m, leveys 4m
- 202310-T1-#9
  - Pähkinäpensas (Corylus avellana) 'Viheraarni' - 1 kpl
  - Hazelnut Bush (Corylus avellana) 'Viheraarni' - Quantity: 1
  - Korkeus 5m, leveys 3m, Tuore, Puolivarjo/Aurinko



