---
title: Koto design system and resources management software
date: 2023-01-23 10:00:00
layout: singletask
aliases:
- /task/kotodesignsystemandresourcesmanagementsoftware
---

## Goal of this task

To define diffrent parts of the software, make the crude design decisions, write tasks to backlog and start the first iteration.

## About the task

Ask _Juuso Vilmunen (juuso@vilmunen.net_) for editing rights.

Don’t use too much time on one topic in the first go. Go through all the parts and start over if you have more time. Let's try to keep an agile approach.

# Content

## What is Koto design system (DS)?

- A data model of physical objects. Part design, how parts are connected and what resources is needed to manufacture parts.
  - Parts are connected with hash links so it is possible to easily share the data without knowing who shares it
  - Everything is defined as a tree of parts whether it is a vehicle or a city
- A network to share data and links to it
- An app to edit the tree and view different parts of trees in a 3D view

## What is Koto resources management (RM)?

- A storage management software
- A network of storages and factories
- A protocol to find which node has or can produce a needed part
  - Finding the shortest path
  - Deciding if a part should be manufactured instead

## How are these connected

Storage system can work only if part IDs ate known and resource usage can be calculated with know resource usages based in the part ID. Design system is only useful if already existing parts can be used easily.

## Some ideas

- Can Openai create simple openscad models? _\- Tested and doesn't work. Maybe with more training._
- Openai can for example give out a data structure with main ports and ports and cities closest to them with an universal ID and coordinates _\- Tested and seems to work_
- Decentralized technology line ssb and ipfs cam be used to share data and bookmarks or releases of part trees as links to the root part. - Should work, but it might be that ssb is not the best option. We should support other ways of releasing new versions like simply RSS.

## Roughly the first iterations

### Version 1

#### Data model version 1

- It should be easily formatted text based data. json is not the best because it can bevwritten in many ways and we should ve able to calculate a hash from content
- A tree model where tree ID changes when a leaf changes in some way
- Flexible enough, but schema versioned. Maybe yaml.
- Decide what format to use so every part can be converted to these:
  - Svg model
  - Resource usage list

#### OpenAI integration

- Ask for data like port and city locations
- Ask for part data like materials used, design and how parts are linked together for a few different things like household devices

#### Simple app (ui or command line)

- Text editor for data
- Handle files in a folder without knowing part IDs
- Output part tree
- Output an image of a tree or single part

#### A simulation of resource management

- Possibly a browser app so people can play with it
- A map where nodes are shown and closest paths drawn
- Simulate large number of nodes in a network

### Backlog

- DS: Root part to Godot node tree
- DS: Structure of release lists
- DS: Release list updated through SSB
- DS: Needed resources for a part
- DS: Calculating root resources usage
- DS: OpenAI training
- RM: Node API
- RM: Query message for part
- RM: Forwarding a query message with depth limit
- RM: Simalation of nodes
- RM: Calcuting score based on resource usage

### Iteration 2023-2

#### Goal

#### Tasks

### Iteration 2023-1

#### Goal

Some basic functionality for design system. Data edited with a text editor and app exports

#### Tasks

- DS: DONE - Basic like API and using SSB and IPFS.
- DS: DONE Gitlab CI pipelines and other things related to build and publishing
- DS: DONE Decide which data format to use (Yaml, JSON...) - using JSON
- DS: DONE (Just adding files to IPFS and letting it do that calculation) - Calculate an ID from a file
- DS: DONE Openscad leaf-part to STL or similar format - export as STL and png works
- DS: DONE Release list from URL
- DS: DONE publishing and loading parts from IPFS
- DS: DONE Launch on export viewer - Kind of a viewer [https://kotocoop.gitlab.io/design-system/kotods-viewer/](https://kotocoop.gitlab.io/design-system/kotods-viewer/)



