---
title: How to use locally cut timber
date: 2023-01-09 10:00:00
layout: singletask
aliases:
- /task/howtouselocallycuttimber
---
# **How to use locally cut timber**

# Goal of this task

The goal is to find out if locally cutting timber is a viable option to use for construction in the plot. Currently we are mostly interested in floor planks cut from spruce of pine.

After we know more we can make a decision if we should do it our self, we buy cutting as a service, or buy lumber from a store.

## About the task

Ask Juuso V (@jeukku) for editing rights.

Don’t use too much time on one topic in the first go. Go through all the parts and start over if you have more time.

# Content

## What kind of woods can we use for which purpose?

### For floors, outside and other usages

### duckbards

## How much different types of construction materials you get from trees?

## Which time of the year is good for cutting the trees down?

## Drying

Different methods. How long does it take for different purposes?

## Floors

Traditional thick planks in floors instead of floor planks with tongue and groove

## Saving compared to buying

## Transport

## Other things to consider

# Conclusions



