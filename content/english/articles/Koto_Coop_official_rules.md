---
title: Koto Cooperative Official Rules - translation unfinished
date: 2020-08-02 11:30:00 +0000
---

ALERT! THIS IS A MACHINE TRANSLATED DOCUMENT. NOT FINISHED!

## § 1 Business name and domicile

The name of the cooperative is Koto Cooperative and the domicile is Hämeenlinna.

## § 2 Purpose and field of activity of the cooperative

The purpose of the activity is to support and facilitate the livelihood of its members by offering them products and services.

In order to achieve its purpose, the cooperative offers the following services: Market and market trade, engineering services and related technical consulting, crop production and related services, construction of residential and other buildings, electricity generation by hydropower and wind power.

The cooperative can also sell services and products outside the cooperative.

## § 3 Distribution of surplus

Any surplus will be used for the expansion of the cooperative.

## § 4 Membership

You can become a member of a cooperative by paying a cooperative fee. All members have the right to vote at the cooperative meeting.

## § 5 Unit fee

The cooperative's board of directors determines the cooperative fee annually.

## § 6 Board of Directors

The cooperative has a board of directors, which consists of one to five members and at least one deputy member, if less than three members are elected to the board.

The term of office of the members of the Board of Directors will continue for the time being.

The new board is elected by the incoming board making a proposal on the composition of the new board. The composition is approved at the cooperative's meeting.

## § 7 Representation of a cooperative

The cooperative is represented by the board. The Board may also grant the nominee a power of attorney or the right to represent the cooperative

## § 8 Cooperative meeting

The members exercise their decision-making power in matters belonging to it in accordance with the law or the rules at the meetings of the cooperative. 

The meeting will be held at the cooperative's domicile. The meeting may also be held in another place where the cooperative has an office. 

The provisions of Chapter 5 of the Cooperatives Act shall be complied with in convening a meeting of a cooperative, the content of the notice of the meeting, the time and manner of convening the meeting and the keeping and sending of meeting documents. The invitation to the meeting will be sent by e-mail to the e-mail address provided by the member or by other electronic means.

## § 9 Annual General Meeting of the Cooperative

The annual meeting of the cooperative must be held within six months of the end of the financial year The meeting shall decide:

* the approval of the accounts
* the use of the surplus shown in the balance sheet
* discharge of the members of the Board of Directors
* the co-operative's auditor

## § 10 Financial year and financial statements

The financial year of the cooperative is a calendar year. Financial statements must be prepared for each financial year. The financial statements must be submitted to the auditor at least one month before the meeting at which the income statement and balance sheet are presented for approval.

## § 11 Auditor and performance auditor

One auditor and a deputy auditor are elected for the cooperative if required by the Auditing Act. The auditor's term of office is valid for the time being.

The cooperative has no obligation to elect an auditor or an operational auditor unless the general meeting of the cooperative decides otherwise.

## § 12 Units

The unit is a designated group of members of the cooperative who follow the rules of the unit set by the cooperative.

The unit elects a responsible person from among its members who acts as a contact person and is obliged to monitor compliance with the unit's rules and to report to the cooperative's board on the activities in accordance with the unit's rules.

All members of the cooperative belong to one unit. Members may request a transfer to another unit and the transfer must be approved by both the cooperative's board of directors and the unit's contact person.

The cooperative may, if it so wishes, dissolve the unit, in which case the members belonging to the unit will be dismissed.

## § 13 Amendment of the rules

The meeting of the cooperative decides on the amendment of the rules. The Board approves the rule changes.

## § 14 General provision

In other respects, the applicable Cooperative Act is complied with.