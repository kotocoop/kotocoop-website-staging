---

title: Koto Co-op organization structure and guidelines

---

## Basics

The core of organizational structure of Koto Co-op is to have a horizontal power hierarchy, self-organization and autonomy of teams and units while having the economy of Co-op staying sustainable. Basic guideline is that Co-op administration does not control the teams or the units but when needed it will intervene. Co-op administration handles the wealth and public relations of the Koto project but otherwise teams and units are free to organize as they please. If there is a problem regarding money or conflict between people that cannot be resolved without intervention of Co-op administration then it will come in to try to resolve these issues.

## Organization of Units

Core idea of organization of Koto Co-op’s Units are that they define their own rules together with the administration of the Co-op. Organization and operation of the unit is controlled by these rules. Basic principle is that all the people that live in a unit should be able to be involved in defining the rules of that unit. Rules should be defined so, that Koto Co-op as well as the unit and its people will benefit from the operation of the unit. As noted earlier, the units should be as autonomous and self-organized as possible while Co-op administration steps in to control things inside the unit only when needed. Co-op however, tries to offer help, guide, and tools for units to organize as efficiently and sustainably as possible.

Possible statuses of the people living in a unit is defined within the rules of the unit. If these statuses legally involve the Co-op then they are defined together with the Co-op administration. This kind of possible statuses that involve the Co-op are tenants, employees of the Co-op, and owners of the Co-op.

Units are established by the Co-op and Co-op owns the property of the units. Co-op can also disband a unit if the unit is not working and is draining funds of the Co-op. Units receive funds from the Co-op and units aim to achieve economical sustainability. Eventually units should create profit that is used by Co-op to fund new units. Revenues and expenses are handled by Co-op administration.

## Future development

In future Koto Co-op want to develop good ways to organize the teams and units within the Co-op. The approaches that are proven good should be informed to units and teams. Co-op tries to offer guidance to teams and units on how to organize and operate efficiently and sustainably.

Koto Co-op recommends all the teams and units of the Co-op to define their activies as clearly as possible. For teams this means defining the purpose, goals, and operating principles of the team. For units this means defining the rules of the unit.

A question that remains yet open is what should the rules of a unit include at minimum?