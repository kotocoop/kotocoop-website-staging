---

title: Structure of Koto Co-op administration team

---

This document is based on discussions at general meeting on 27.10.2020.

The minimum roles that should be filled so that Koto Co-op can work as an organization would be following roles:

1. CEO
2. Accounting
3. Sales and marketing
4. Communication

There are several responsibilities that fall under these roles. These responsibilities are listed under the roles as subcategories. These subcategories represent new roles and teams when Koto Co-op grows and administration team expands.

1\.CEO

1\.1 Responsive of Units

2\. Accounting,

2\.1 Business Law

2\.2. Ecological Sustainability and material handling

3\. Sales and marketing

3\.1 Product development

3\.2 Technology

4\. Communication /PR

4\.1 Human resources

Also there should be at some point a contact person for all the Koto people that is not part of the administration team but is involved as a representative of people living in units and working in teams.