---
title: Koto Co-op code of conduct
date: 2021-08-21 12:30:00 +0000

aliases:

- /code_of_conduct
- /coc
---

* Respect other people’s physical and mental space. Do not touch anyone without asking permission. Remember, you cannot know another person’s boundaries without asking. Also, make room for yourself if needed.
* Treat others the way you would like to be treated.
* Do not make assumptions based on appearance or action. Be respectful towards all. 
* Don't make assumptions about anyone's sexuality, gender, nationality, ethnicity, religion, values, socio-economic background, health, or able-bodiedness
* Don't humiliate or embarrass others. Refrain from gossiping and judging others’ appearance.
* Do not repeat in your speech, actions or behavior any stereotypes based on another person’s qualities or characteristics.
* Give room - make sure everyone has the opportunity to participate in the conversation. Also respect the privacy of others and treat sensitive topics with respect.
* You can give appropriate and constructive feedback on inappropriate behavior. Listen to any feedback given to you. Try to be open about the feedback you receive and take it into account in the future. Apologize if you have hurt someone.
* If you notice anyone else acting inappropriately or in any other way acting against Koto Co-op principles, intervene in the situation.

